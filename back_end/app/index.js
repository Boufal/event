const express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    winston = require('winston'),
    expressWinston = require('express-winston'),
    config = require('./config'),
    cors = require('cors'),
    app = express();

//CORS
const options = {
    origin: ['http://localhost:3000'],
    methods: ["POST"],
    credentials: true,
    maxAge: 3600
};
app.options('*', cors(options))
app.use(cors(options));

//PARSER
app.use(bodyParser.json({type: 'application/json'}));

//DB
mongoose.connect(
    'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.databaseName,
    {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//ROUTES
app.use(require('./event/routes/Attendant'));

//ERROR LOGGER
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.File({
            name: 'error-log',
            filename: './back_end/logs/error.log',
            level: 'error'
        })]
}));

//ERROR HANDLER
app.use(require('./errors/handler'))

//Content Type
app.use(function (req, res, next) {
    res.contentType('application/json');
    next();
});

module.exports = app
