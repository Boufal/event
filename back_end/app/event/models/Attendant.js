const mongoose = require('mongoose')
const Schema = mongoose.Schema

const attendantSchema = new Schema({
        firstName: {
            type: String,
            required: true
        },
        secondName: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true
        },
        eventDate: {
            type: Date,
            required: true
        }
    }, {
        timestamps: true
    }
)

module.exports = mongoose.model('Attendant', attendantSchema)



