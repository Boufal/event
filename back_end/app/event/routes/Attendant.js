const router = require('express').Router()
const Attendant = require('../models/Attendant')
const {body, validationResult} = require('express-validator');

router.post('/attendant',
    [
        body('firstName')
            .exists().withMessage('Missing field').bail()
            .notEmpty().withMessage('Field must not be empty').bail()
            .trim(),
        body('secondName')
            .exists().withMessage('Missing field').bail()
            .notEmpty().withMessage('Field must not be empty').bail()
            .trim(),
        body('email')
            .exists().withMessage('Missing field').bail()
            .notEmpty().withMessage('Field must not be empty').bail()
            .isEmail().withMessage('Wrong Email').bail(),
        body('eventDate')
            .exists().withMessage('Missing field').bail()
            .notEmpty().withMessage('Field must not be empty').bail()
            .isDate().withMessage('Field must be a date').bail()

    ], async (req, res, next) => {
        const errors = validationResult(req)

            try {
                if (!errors.isEmpty()) {
                    res.status(400).json({...{success: false}, ...errors }).send();
                } else {

                    const { firstName, secondName, email, eventDate } = req.body //safe?
                    const attendant = new Attendant({ firstName, secondName, email, eventDate })
                    await attendant.save()

                    res.status(201).json({
                        success: true, msg: 'Successfully registered new attendant',
                        attendant: {
                            email: attendant.email,
                            eventDate: attendant.eventDate,
                            firstName: attendant.firstName,
                            secondName: attendant.secondName
                        }
                    });
                }
            } catch (error) {
                 next(error)
            }
    })
module.exports = router