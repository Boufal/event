const chai = require('chai'), chaiHttp = require('chai-http');
const Attendant = require('../app/event/models/Attendant')
const mongoose = require('mongoose');
chai.use(chaiHttp);
const expect = chai.expect;

describe('Attendant tests', function () {
    const app = require('./../app/index.js');

    it('Attendant post positive test', done => {
        chai
            .request(app)
            .post("/attendant")
            .send({firstName: 'Olaf', secondName: 'Rudolf', email: 'lol@gmail.com', eventDate: '1900-01-01'})
            .end((err, res) => {
                expect(res).to.have.status(201);
                expect(res).to.be.json

                Attendant.find({firstName: 'Olaf', secondName: 'Rudolf'}, function (error, result) {
                    const addedAttendant = result[0]
                    expect(JSON.stringify(res.body.attendant)).to.equal(JSON.stringify({
                        email: addedAttendant.email,
                        eventDate: addedAttendant.eventDate,
                        firstName: addedAttendant.firstName,
                        secondName: addedAttendant.secondName
                    }))
                    expect(res.body.success).to.be.true
                })

                done();
            });
    });

    it('Attendant missing field test', done => {
        chai
            .request(app)
            .post("/attendant")
            .send({firstName: 'Olaf', secondName: 'Rudolf', email: 'lol@gmail.com'})
            .end((err, res) => {

                expect(res).to.have.status(400);
                expect(res).to.be.json
                expect(res.body.success).to.be.false
                expect(JSON.stringify(res.body.errors)).to.equal(
                    JSON.stringify(
                        [{msg:"Missing field",param:"eventDate",location:"body"}]
                    )
                )

                done();
            });
    });

    it('Attendant empty field test', done => {
        chai
            .request(app)
            .post("/attendant")
            .send({firstName: '', secondName: 'Rudolf', email: 'lol@gmail.com', eventDate: '1900-01-01'})
            .end((err, res) => {

                expect(res).to.have.status(400);
                expect(res).to.be.json
                expect(res.body.success).to.be.false
                expect(JSON.stringify(res.body.errors)).to.equal(
                    JSON.stringify(
                         [{value: "", msg: "Field must not be empty", param: "firstName", location: "body"}]
                    )
                )
                done();
            });
    });


    after(function () {
        mongoose.connection.collections['attendants'].drop();
    });
});