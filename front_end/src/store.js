import {applyMiddleware, createStore} from "redux";
import Redux from "redux-thunk"
import thunk from 'redux-thunk';
import reducer from './reducers';

const store = createStore(reducer, applyMiddleware(thunk));

export default store;